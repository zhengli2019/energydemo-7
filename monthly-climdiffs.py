''' Relative Climatology Differences
    ================================
    Consider a single simulation with the idea to pick individual simulations 
    apart first, rather than naïvely loop over a bunch of simulations and then 
    work backwards for explanations when our results don't match intuitions.

    Output is a panel plot of relative differences between monthly 
    climatologies. 
                                                                            '''
import matplotlib.pyplot as plt             
import matplotlib as mpl
import seaborn as sns
import xarray as xr
import pandas as pd
import numpy as np
import cartopy
import sys

from cartopy.io.img_tiles import Stamen
from cdo import *
cdo = Cdo()

metprm = sys.argv[1]  #'pr'

''' Along with libraries above will pull in some possible fancy map 
    backgrounds (each time draw a map the program makes and online quiery to 
    pull in the image, so can be a bit slow).  
                                                                            '''
stamen_terrain = Stamen('terrain-background') 
stamen_toner   = Stamen('toner-background')


def set_values(hsim_path,psim_path,metprm) :
    ''' 
    Read in files and calculate tables of relative differences that can 
    be passed to plotting routines. 

    Need to pick years out of what we have access to that we want to make 
    a climatology from. Should use the same number of years in both for 
    consistancy (if not need to be very careful about how significance of 
    change is computed to account for the uncertainy in the means being 
    different). 
    * Here we have hard coded the periods between 1976:2005 and 
      2021:2050. That should be adjusted if desired. 


    Variables
    ---------
    hsim_path : path to historical simulation (str)
    psim_path : path to projection simulation (str)
    metprm : name of meteorological parameter being investigated (str)
    '''

    ## Read in data
    H = xr.open_dataset(hsim_path)
    P = xr.open_dataset(psim_path)

    ## Change units
    if metprm == 'pr' : 
        H.pr.values = 86400*H.pr.values
        P.pr.values = 86400*P.pr.values
        H.pr.attrs['units'] = 'mm/month'
        P.pr.attrs['units'] = 'mm/month'

    ## Monthly Climatologies
    idx_clim = [x.year 
                in range(1976,2006) 
                for x in H.time.values[:]]
    H = H.sel(time=idx_clim)

    idx_clim = [x.year 
                in range(2021,2051) 
                for x in P.time.values[:]]
    P = P.sel(time=idx_clim)

    nyrs = len(
        np.unique(
            [x.year for x in H.time.values]
        ))

    H_clim = H.groupby('time.month').mean('time')
    H_clim['StndDev'] = H.groupby('time.month').std('time')[metprm]
    H_clim['StndErr'] = H_clim['StndDev']/np.sqrt(nyrs)

    nyrs = len(
        np.unique(
            [x.year for x in P.time.values]
        ))

    P_clim = P.groupby('time.month').mean('time')
    P_clim['StndDev'] = P.groupby('time.month').std('time')[metprm]
    P_clim['StndErr'] = P_clim['StndDev']/np.sqrt(nyrs)


    ''' 
    Now calculate the relative difference between the climatology means in 
    the projection and the 'base line' values. As well, can can calculate 
    how many sigma levels we need to exapand the standard errors of those 
    means in order to get the raw differences to overlap, which here 
    functions as an addhoc probability of the differences being significant. 
    * Would be better to estimate this through bootstraping, and maybe to 
      do the same with the standard deviation values as well.
    '''


    D = H_clim.copy(deep=True)                   # create a new copy of grid
    D = D.drop([metprm,'StndDev','StndErr'])     # make it an empty copy

    D['raw_diff'] = (                            # diff climatologies
        P_clim[metprm] - H_clim[metprm] )
    
    D['rel_diff'] = D.raw_diff/H_clim[metprm]    # diff in mean
                                                 # ... as % of historical
    
    D['dev_diff'] = (                            # diff in variation
        P_clim.StndDev - H_clim.StndDev )        # ... as % of historical
    D['rel_dev'] = D.dev_diff/H_clim.StndDev

    ''' Record how many stnd err devs between the climatology means '''
    D['sig_diff'] = D.raw_diff.copy(deep=True)   
    D['sig_diff'].values = (D.sig_diff.values*0)+4  
    for m in range(1,13) : 
        for sig_lev in range(4,0,-1) : 
            D['sig_diff'].sel(month=m).values[
                np.abs(D.raw_diff.sel(month=m)) 
                <= (sig_lev*P_clim.StndErr.sel(month=m)
                    + sig_lev*H_clim.StndErr.sel(month=m))] = sig_lev
        

    return(D)


def show_range(D) :
    '''
    Write out the range of values in the difference statistics

    Variables
    ---------
    D : data file containing difference statistics  (xarray)
    '''
    print('---------------------------------------')
    print('min mean diff (as %): ',
          str(np.round(D.rel_diff.min().values,2)*100),'%')
    print('max mean diff (as %): ',
          str(np.round(D.rel_diff.max().values,2)*100),'%')
    print()
    print('min stnd.dev. diff (as %): ',
          str(np.round(D.rel_dev.min().values,2)*100),'%')
    print('max stnd.dev. diff (as %): ',
          str(np.round(D.rel_dev.max().values,2)*100),'%')
    print('---------------------------------------')

def map_values(D) :
    '''
    Draw values on a map.

    Variables
    ---------
    D : data file containing statistics to be plot (xarray)
    '''

    color_map = 'PuOr'
    variable_name = 'Precipitation Percent Change (%)'
    min_lat =  50.0 #D.lat.values.min()
    max_lat =  54.0 #D.lat.values.max()
    min_lon =  -2.0 #D.lon.values.min()
    max_lon =   6.0 #D.lon.values.max()
    clon = min_lon + (max_lon - min_lon)/2.0
    clat = min_lat + (max_lat - min_lat)/2.0 
    month_lab=['January','February', 'March','April',
               'May','June','July','August',
               'September','October','November','December']
    
    month = range(1,13);

    fig,axs = plt.subplots(            # add 12 subplots and remove axs
        frameon=False, nrows=3,
        ncols=4,figsize=(26, 18),
        subplot_kw={'xticks': [], 'yticks': []}
    ) 

    fig.subplots_adjust(               # adjust subplots positions and spaces
        left=0.03, right=0.97,
        hspace=0.06, wspace=0.05) 

    for i in range (0,3):
        for j in range (0,4):
            axs[i,j].spines['left'].set_visible(False)
            axs[i,j].spines['right'].set_visible(False)
            axs[i,j].spines['bottom'].set_visible(False)
            axs[i,j].spines['top'].set_visible(False)

    for i in range(0,12):
        chart = fig.add_subplot(
            3, 4, i+1,projection=cartopy.crs.LambertConformal(
                central_longitude=clon,central_latitude=clat))

        _ = chart.set_extent(
            [min_lon, max_lon, min_lat, max_lat], 
            crs=cartopy.crs.PlateCarree())

        _ = chart.add_image(stamen_terrain,8,interpolation='spline16')

        m0 = chart.scatter(
            D.lon,D.lat,
            transform=cartopy.crs.PlateCarree(),
            cmap=color_map,    
            c=D.rel_diff.sel(month=month[i])*100,
            s=D.sig_diff.sel(month=month[i])*3,    
            vmin=-25,vmax=25)   

        rd = D.rel_dev.sel(month=month[i])*100
        m1 = rd.plot.contour(
            'lon','lat',ax=chart,
            levels=np.arange(-90,30,30),
            linewidths=4,linestyles='dashed',
            transform=cartopy.crs.PlateCarree(),
            add_colorbar=False,
            vmin=-25,vmax=0,
            alpha=0.9,cmap='Greys')

        m2 = rd.plot.contour(
            'lon','lat',ax=chart,
            levels=np.arange(0,120,30),
            linewidths=4,linestyles='solid',
            transform=cartopy.crs.PlateCarree(),
            add_colorbar=False,
            vmin=0,vmax=25,
            alpha=0.9,cmap='Greys')

        _ = plt.title(month_lab[i],fontsize=14)


    fig.suptitle('_'.join(fname_hsim.split('_')[:-5]), fontsize=16)
    plt.colorbar(
        m0, cax = plt.axes([0.98, 0.125, 0.015, 0.76]),
        label=variable_name)
    plt.savefig('test_image.png')


    
# ________________________________________________________________________
# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::



''' Pull data
    ---------
    Data paths and file names will vary. 
                                                                            '''
datapath = '/home/ubuntu/eucordex/NorthSea'

''' Different met-parameters will have different associated aggregations. 
    * May want to edit so that aggregation is set based on a seperate 
      external argument, since in future we might have multiple aggregations
      performed on the same parameters.

                                                                            '''
if metprm == 'pr' :
    aggr = 'monsum_' 
else :
    aggr = ''
fname_hsim = (aggr+'NorthSea_'+metprm+'_EUR-11_'
              +'NCC-NorESM1-M_'
              +'historical_r1i1p1_'
              +'SMHI-RCA4_v1_day.nc')
fname_psim = ('monsum_NorthSea_pr_EUR-11_'+'NCC-NorESM1-M_'
              +'rcp85_r1i1p1_'
              +'SMHI-RCA4_v1_day.nc')


D = set_values(
    hsim_path=datapath+'/'+fname_hsim,
    psim_path=datapath+'/'+fname_psim,
    metprm='pr')
show_range(D) 
map_values(D)

# ==:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:==


